package ru.khusnullin.eduNotes.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.khusnullin.eduNotes.domain.User;

public interface UserRepo extends JpaRepository<User, Long> {
    User findByUsername(String username);

    User findByActivationCode(String code);
}
