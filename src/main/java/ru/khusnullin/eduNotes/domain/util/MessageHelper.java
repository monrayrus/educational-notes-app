package ru.khusnullin.eduNotes.domain.util;

import ru.khusnullin.eduNotes.domain.User;

public abstract class MessageHelper {
	
	public static String getAuthorName(User author) {
		return author != null ? author.getUsername() : "<none>";
	}
}
