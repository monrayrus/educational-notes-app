package ru.khusnullin.eduNotes.domain;

import org.hibernate.validator.constraints.Length;

import ru.khusnullin.eduNotes.domain.util.MessageHelper;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
public class Message {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @NotBlank(message = "Text must not be blank")
    @Length(max = 2048, message = "Text must be less than 2kb")
    private String text;

    @NotBlank(message = "Tag must not be blank")
    @Length(max = 255, message = "Tag must be less than 255b")
    private String tag;

    private String filename;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User author;
    
    @ManyToMany
    @JoinTable(name = "message_likes", joinColumns = {@JoinColumn(name="message_id")}, inverseJoinColumns = {@JoinColumn(name="user_id")})
    private Set<User> likes = new HashSet<>();

    public Message() {
    }

    public String getAuthorName() {
        return MessageHelper.getAuthorName(author);
    }

    public Message(String text, String tag, User user) {
        this.author = user;
        this.text = text;
        this.tag = tag;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }
    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

	public Set<User> getLikes() {
		return likes;
	}

	public void setLikes(Set<User> likes) {
		this.likes = likes;
	}
    
    
}