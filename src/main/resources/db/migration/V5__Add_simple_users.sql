insert into usr (id, username, password, active)
values (2, 'mike', crypt('mike', gen_salt('bf',8)), true);

insert into user_role (user_id, roles)
values (2, 'USER');

insert into usr (id, username, password, active)
values (3, 'test', crypt('test', gen_salt('bf',8)), true);

insert into user_role (user_id, roles)
values (3, 'USER');
