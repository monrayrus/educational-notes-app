1.  в терминале IDEA пишем следующее: "C:\Program Files\PostgreSQL\14\bin\psql.exe" -p 5432 -U postgres

(если иной путь до данного файла, соответственно, ставьте его. 5432 - порт, на котором висит постгрес. 
postgres - имя пользователя БД)


2. Если всё нормально и в терминале появилась надпись postgres=#, тогда вставляем следующее:
   SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = 'edunotesa' AND pid <> pg_backend_pid();

(этой командой мы прерываем все сессии, которые сейчас взаимодействуют с данной БД)

3. Затем вводим:
   drop database edunotesa; create database edunotesa;
   
   
   Чтобы после добавления сообщения шаблонизатор не крэшился нужно те же изменения что были сделаны
в методе main() класса MainController (он же MessageController), сделать в MainController, в методе
@PostMapping("/main")
public String add(...)

А именно -

(1) добавить в MainController.add() параметр
@PageableDefault(sort = { "id" }, direction = Sort.Direction.DESC) Pageable pageable

(2) заменить в этом же методе строки
Iterable<Message> messages = messageRepo.findAll();
model.addAttribute("messages", messages);

на

model.addAttribute("url", "/main");
Page<MessageDto> page = messageService.messageList(pageable, "", user); // пустой фильтр
model.addAttribute("page", page);